import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileWriter {

    public static void writeTask(String task, int taskNo){

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("Task" + taskNo + ".txt"), StandardCharsets.UTF_8))) {
            writer.write(task);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
