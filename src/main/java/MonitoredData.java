public class MonitoredData {

    private final String startTime;
    private final String endTime;
    private final String activity;

    public MonitoredData(String startTime, String endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    @Override
    public String toString() {
        return "MonitoredData{" +
                "startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", activity='" + activity + '\'' +
                '}';
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getActivity() {
        return activity;
    }

    public long getSeconds(String time) {
        String[] buf = time.split(":");
        return Long.parseLong(buf[0]) * 3600 + Long.parseLong(buf[1]) * 60 + Long.parseLong(buf[2]);
    }

}
