import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Parser {

    List<MonitoredData> parseData (){
        File file = new File("Activities.txt");
        List<MonitoredData> monitoredDataList = new ArrayList<>();

        try {
            Files.lines(file.toPath()).
                    map(l -> l.split("\t", 4)).forEach(
                            a -> monitoredDataList.add(new MonitoredData(a[0], a[2], a[3].replaceAll("\\s+",""))));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return monitoredDataList;
    }

}
