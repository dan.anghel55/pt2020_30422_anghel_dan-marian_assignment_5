import java.util.List;

public class Task1 {

    Task1 (){

        List<MonitoredData> monitoredDataList = new Parser().parseData();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TASK 1:\n\n");
        monitoredDataList.forEach(l -> stringBuilder.append(l).append("\n"));

        FileWriter.writeTask(String.valueOf(stringBuilder), 1);
        System.out.println("Task 1 performed successfully!");
    }

}
