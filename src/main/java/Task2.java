import java.util.List;
import java.util.stream.Stream;

public class Task2 {

    Task2 (){

        List<MonitoredData> monitoredDataList = new Parser().parseData();

        Stream<String> startTimeStream = monitoredDataList.stream().map(m -> m.getStartTime().substring(8, 10));
        Stream<String> endTimeStream = monitoredDataList.stream().map(m -> m.getEndTime().substring(8, 10));
        Stream<String> resultingStream = Stream.concat(startTimeStream, endTimeStream);

        FileWriter.writeTask("TASK 2:\n\nThe number of distinct days is: " + resultingStream.distinct().count(), 2);
        System.out.println("Task 2 performed successfully!");
    }

}
