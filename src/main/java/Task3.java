import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class Task3 {

    Task3 (){

        Map<String, Integer> collect = new Parser().parseData()
                .stream()
                .map(MonitoredData::getActivity)
                .collect(groupingBy(Function.identity(), summingInt(e -> 1)));

        LinkedHashMap<String, Integer> countActivitiesSorted = collect.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (v1, v2) -> {
                            throw new IllegalStateException();
                        },
                        LinkedHashMap::new
                ));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TASK 3:\n\n");
        countActivitiesSorted.entrySet().forEach(e -> stringBuilder.append(e).append("\n"));

        FileWriter.writeTask(String.valueOf(stringBuilder), 3);
        System.out.println("Task 3 performed successfully!");
    }

}
