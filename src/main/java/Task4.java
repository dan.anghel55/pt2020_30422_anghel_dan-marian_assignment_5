import java.util.Map;
import java.util.stream.Collectors;

public class Task4 {

    Task4 (){

        Map<Integer, Map<String, Long>> timeActivityMap = new Parser().parseData()
                .stream()
                .collect(Collectors.groupingBy(
                        element -> Integer.parseInt(element.getStartTime().substring(8, 10)),
                        Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TASK 4:\n\n");
        timeActivityMap.entrySet().forEach(e -> stringBuilder.append(e).append("\n"));

        FileWriter.writeTask(String.valueOf(stringBuilder), 4);
        System.out.println("Task 4 completed successfully!");
    }

}
