import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Task5 {

    private final static long ONE_SECOND = 1000;
    private final static long ONE_MINUTE = ONE_SECOND * 60;
    private final static long ONE_HOUR = ONE_MINUTE * 60;
    private final static long ONE_DAY = ONE_HOUR * 24;

    Task5 () {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Map<String, Long> collect = new Parser().parseData()
                .stream()
                .collect(Collectors.groupingBy(
                        MonitoredData::getActivity,
                        Collectors.summingLong(
                                e -> {
                                    try {
                                        return simpleDateFormat.parse(e.getEndTime()).getTime() - simpleDateFormat.parse(e.getStartTime()).getTime();
                                    } catch (ParseException parseException) {
                                        parseException.printStackTrace();
                                    }
                                    return 0;
                                })));


        LinkedHashMap<String, Long> activityDuration = collect.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (v1, v2) -> {
                            throw new IllegalStateException();
                        },
                        LinkedHashMap::new
                ));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TASK 5:\n\n");
        activityDuration.forEach((key, value) -> stringBuilder.append(key).append(" = ").append(millisToLongDHMS(value)).append("\n"));

        FileWriter.writeTask(String.valueOf(stringBuilder), 5);
        System.out.println("Task 5 completed successfully!");
    }

    private String millisToLongDHMS(long duration) {
        StringBuilder res = new StringBuilder();
        long temp;
        if (duration >= ONE_SECOND) {
            temp = duration / ONE_DAY;
            if (temp > 0) {
                duration -= temp * ONE_DAY;
                res.append(temp).append(" day").append(temp > 1 ? "s" : "")
                        .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_HOUR;
            if (temp > 0) {
                duration -= temp * ONE_HOUR;
                res.append(temp).append(" hour").append(temp > 1 ? "s" : "")
                        .append(duration >= ONE_MINUTE ? ", " : "");
            }

            temp = duration / ONE_MINUTE;
            if (temp > 0) {
                duration -= temp * ONE_MINUTE;
                res.append(temp).append(" minute").append(temp > 1 ? "s" : "");
            }

            if (!res.toString().equals("") && duration >= ONE_SECOND) {
                res.append(" and ");
            }

            temp = duration / ONE_SECOND;
            if (temp > 0) {
                res.append(temp).append(" second").append(temp > 1 ? "s" : "");
            }
            return res.toString();
        } else {
            return "0 second";
        }
    }

}
