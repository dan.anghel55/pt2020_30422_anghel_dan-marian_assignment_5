import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;


public class Task6 {

    public Task6() {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Map<String, Integer> count5MinActivities = new Parser().parseData()
                .stream()
                .filter(e -> {
                    try {
                        return (simpleDateFormat.parse(e.getEndTime()).getTime() - simpleDateFormat.parse(e.getStartTime()).getTime()) < 300000;
                    } catch (ParseException parseException) {
                        parseException.printStackTrace();
                    }
                    return false;
                })
                .map(MonitoredData::getActivity)
                .collect(groupingBy(Function.identity(), summingInt(e -> 1)));

        Map<String, Integer> countAllActivities = new Parser().parseData()
                .stream()
                .map(MonitoredData::getActivity)
                .collect(groupingBy(Function.identity(), summingInt(e -> 1)));

        List<String> activities = count5MinActivities.entrySet()
                .stream()
                .filter(e -> (double)(e.getValue()/countAllActivities.get(e.getKey())) >= 0.9)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("TASK 6:\n\n");
        activities.forEach(e -> stringBuilder.append(e).append("\n"));

        FileWriter.writeTask(String.valueOf(stringBuilder), 6);
        System.out.println("Task 6 completed successfully!");
    }
}

    /*List<String> count5MinActivities = new Parser().parseData()
            .stream()
            .filter(e -> {
                try {
                    return (simpleDateFormat.parse(e.getEndTime()).getTime() - simpleDateFormat.parse(e.getStartTime()).getTime()) < 300000;
                } catch (ParseException parseException) {
                    parseException.printStackTrace();
                }
                return false;
            })
            .map(MonitoredData::getActivity)
            .distinct()
            .collect(Collectors.toList());*/
